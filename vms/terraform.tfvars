# project name
projectname = "net-lab-infra"

# OS image
# sourceimage = "$HOME/.images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2"
# sourceimage = "$HOME/.images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2"
sourceimage = "https://download.fedoraproject.org/pub/fedora/linux/releases/33/Cloud/x86_64/images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2"

# the base image is the source image for all VMs created from it
baseimagediskpool = "default"


# domain and network settings
domainname  = "local"
networkname = "default" # default==NAT

subnets       = ["10.16.4.0/24", "10.16.5.0/24", "10.16.6.0/24", "10.16.7.0/30"]
network_names = ["inner", "outer", "dmz", "router"]
dhcp          = [true, true, false, false]

# host-specific settings
# RAM in bytes
# disk size in bytes (disk size must be greater than source image virtual size)
hosts = {
  "h_victim" = {
    name        = "h_victim",
    vcpu        = 1,
    memory      = "768",
    diskpool    = "default",
    disksize    = "11000000000",
    mac         = "00:00:00:13:37:22",
    sourceimage = "https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2",
    category    = "host-victim",
    network = {
      "inner" = {
        name    = "inner",
        mode    = "dhcp",
        address = ["10.16.4.7"],
        mac     = "00:04:00:13:37:22",
      },
    }
  },
  "h_attacker" = {
    name     = "h_attacker",
    vcpu     = 1,
    memory   = "1024",
    diskpool = "default",
    disksize = "10370000000",
    mac      = "00:00:00:13:37:23",
    # sourceimage = "https://download.fedoraproject.org/pub/fedora/linux/releases/34/Cloud/x86_64/images/Fedora-Cloud-Base-34-1.2.x86_64.qcow2",
    sourceimage = "/var/lib/libvirt/images/Fedora-Cloud-Base-34-1.2.x86_64.qcow2",
    category    = "host-attacker",
    network = {
      "outer" = {
        name    = "outer",
        mode    = "dhcp",
        address = ["10.16.5.4"],
        mac     = "00:05:00:13:37:23",
      },
    }
  },
  "r_edge" = {
    name     = "r_edge",
    vcpu     = 1,
    memory   = "768",
    diskpool = "default",
    disksize = "4300000000",
    mac      = "00:00:00:13:37:24",
    # sourceimage = "https://download.fedoraproject.org/pub/fedora/linux/releases/33/Cloud/x86_64/images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2",
    sourceimage = "/var/lib/libvirt/images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2",
    category    = "router",
    network = {
      "dmz" = {
        name    = "dmz",
        mode    = "route",
        address = ["10.16.6.1"],
        mac     = "00:06:00:13:37:24",
      },
      "router" = {
        name    = "router",
        mode    = "route",
        address = ["10.16.7.1"],
        mac     = "00:07:00:13:37:24",
      },
    }
  },
  "r_upstream" = {
    name     = "r_upstream",
    vcpu     = 1,
    memory   = "768",
    diskpool = "default",
    disksize = "4300000000",
    mac      = "00:00:00:13:37:25",
    # sourceimage = "https://download.fedoraproject.org/pub/fedora/linux/releases/33/Cloud/x86_64/images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2",
    sourceimage = "/var/lib/libvirt/images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2",
    category    = "router",
    network = {
      "outer" = {
        name    = "outer",
        mode    = "dhcp",
        address = ["10.16.5.3"],
        mac     = "00:05:00:13:37:25",
      },
      "router" = {
        name    = "router",
        mode    = "route",
        address = ["10.16.7.2"],
        mac     = "00:07:00:13:37:25",
      },
    },
  },
  "h_defender" = {
    name     = "h_defender",
    vcpu     = 1,
    memory   = "2048",
    diskpool = "default",
    disksize = "10370000000",
    mac      = "00:00:00:13:37:26",
    # sourceimage = "https://download.fedoraproject.org/pub/fedora/linux/releases/34/Cloud/x86_64/images/Fedora-Cloud-Base-34-1.2.x86_64.qcow2",
    sourceimage = "/var/lib/libvirt/images/Fedora-Cloud-Base-34-1.2.x86_64.qcow2",
    category    = "host-defender",
    network = {
      "dmz" = {
        name    = "dmz",
        mode    = "route",
        address = ["10.16.6.4"],
        mac     = "00:06:00:13:37:26",
      },
    },
  },
}

